﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart( {
        chart: {
            renderTo: 'container1',
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([62.74, 10.57, 7.23]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0,0,0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 2000);
                }
            }
        },
        title: {
            text: 'BÁO CÁO 114',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['nhà dân', 'khu công nghiệp', 'cháy nổ lớn']
        },
        yAxis: {
            max:70,
            title: {
                text: 'Số vụ cháy'
            }
        },
        colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Cháy nhà dân",
                    "y": 62.74
                },
                {
                    "name": "Khu công nghiệp",
                    "y": 10.57
                },
                {
                    "name": "Cháy nổ lớn",
                    "y": 7.23
                }
              ]
          }
        ],
        
    });
    
});