﻿$(function () {
    var chart = new Highcharts.chart({
        chart: {
            renderTo: 'container2',
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([34.2, 22.3, 2.5, 31.1,0.3,0.1,2.2,0.3,3.2,0.2,0.1,3.5]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0, 0, 0,0,0,0,0,0,0,0,0,0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 1700);
                }
            }
        },
        title: {
            text: 'BÁO CÁO 115',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['']
        },
        yAxis: {
            max: 35,
            title: {
                text: ''
            }
        },
        colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "TNGT nghiêm trọng",
                    "y": 34.2,
                },
                {
                    "name": "Tai nạn lao động",
                    "y": 22.3
                },
                {
                    "name": "Bỏng",
                    "y": 2.5
                },
                {
                    "name": "Ngã",
                    "y": 31.1
                },
                {
                    "name": "Ngộ độc",
                    "y": 0.3
                },
                {
                    "name": "Tự tử",
                    "y": 0.1
                },
                {
                    "name": "Động vật cắn, đốt",
                    "y": 2.2
                },
                {
                    "name": "Đuối nước",
                    "y": 0.3
                },
                {
                    "name": "Bạo hành",
                    "y": 3.2
                },
                {
                    "name": "Điện giật",
                    "y": 0.2
                },
                {
                    "name": "Bom mìn",
                    "y": 0.1
                },
                {
                    "name": "Khác",
                    "y": 3.5
                }

              ]
          }
        ],
        
    });
    
});