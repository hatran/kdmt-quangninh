﻿
var lat = 10.773448; 
var lon = 106.705328;
var map = new L.Map('map', {

        zoom: 18,
        minZoom: 4,
});

var lstPolice = [{
        name: 'Cảnh sát PCCC Quận 3 ',
        address: 'Pccc Quận 3, 103 Lý Chính Thắng, Phường 8, Quận 3, Hồ Chí Minh'
    },{
        name: 'Cảnh Sát Pccc Quận 1',
        address: '258 Đường Trần Hưng Đạo, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh'
    },{
        name: 'Cảnh Sát Pccc Quận 4',
        address: '196 Tôn Thất Thuyết, Phường 3, Quận 4, Hồ Chí Minh'
    }];

    var lstHospital = [{
        name: 'Bệnh viện Bình Dân',
        address: '408 Điện Biên Phủ, Phường 11, Quận 10, Hồ Chí Minh'
    },{
        name: 'Bệnh viện Nguyễn Trãi',
        address: '314 Nguyễn Trãi, Phường 8, Quận 5, Hồ Chí Minh'
    }]

    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 18
    });

    map.addLayer(layer);

    map.setView([lat, lon], 16);

    var FireIcon = L.icon({
        iconUrl: 'images/fire-2-32_2.gif',
        iconSize: [36, 36], // size of the icon
    });

    var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);

    var pccc01 = new L.Routing.control({
    waypoints: [
        L.latLng(10.7896, 106.6854),        
        L.latLng(lat,lon)
    ],
    createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
            return L.marker(L.latLng(10.7896, 106.6854))
                .bindPopup('Tuyến đường từ: ' + lstPolice[0].name + 'di chuyển thông thoáng.'+' <br>'+' Sau 8 phút có thể tới nơi');
        },
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'green', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);


    var pccc02 = new L.Routing.control({
    waypoints: [
        L.latLng(10.7611, 106.6893),        
        L.latLng(lat,lon)
    ],
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'red', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

    var pccc03 = new L.Routing.control({
    waypoints: [
        L.latLng(10.7568, 106.7178),        
        L.latLng(lat,lon)
    ],
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'blue', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);
    

    var cuuThuogn01 = new L.Routing.control({
    waypoints: [
        L.latLng(10.7735, 106.6796),       
        L.latLng(lat,lon)
    ],
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'green', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

    var cuuThuogn02 = new L.Routing.control({
    waypoints: [
        L.latLng(10.7566, 106.6750),        
        L.latLng(lat,lon)
    ],
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'red', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);





    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function openModal(idModal) {
        document.getElementById(idModal).style.display = "block";
    }
