﻿var lat = 20.9537;
var lon = 107.0824;
var map = new L.Map('map', {

        zoom: 15,
        minZoom: 6,
});

    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 16
    });

    // add the layer to the map
    map.addLayer(layer);

    var cuuthuong = getParameterByName('ct');
    var cuuhoa = 1
    var chihuy = getParameterByName('chh');

    var lstGarbagePoint1 = [{
        mathung: 'TH00001',
        name: 'Điểm tập kết rác thải Lán Bè',
        lat: '20.9519',
        lon: '107.0719',
        status: '80 - 100%',
        address: 'Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00011',
        name: 'Điểm tập kết rác thải đường Đồng Hồ Lán Bè',
        lat: '20.95290',
        lon: '107.09945',
        status: '80 - 100%',
        address: 'Đồng Hồ, Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00031',
        name: 'Điểm tập kết rác thải đường 25 - 4',
        lat: '20.9521',
        lon: '107.0807',
        status: '80 - 100%',
        address: 'Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH0008',
        name: 'Điểm tập kết rác thải Hàng Than - Ba Đèo',
        lat: '20.95279',
        lon: '107.07323',
        status: '80 - 100%',
        address: 'Ba Đèo, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00022',
        name: 'Điểm tập kết rác thải đường Lê Lợi',
        lat: '20.9590',
        lon: '107.0749',
        status: '80 - 100%',
        address: 'Lê Lợi, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00033',
        name: 'Điểm tập kết rác thải đường Lê Lai',
        lat: '20.9577',
        lon: '107.0838',
        status: '80 - 100%',
        address: 'Lê Lai, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },
    {
        mathung: 'TH00026',
        name: 'Điểm tập kết rác thải số 2 Đường Lê Thánh Tông',
        lat: '20.9563',
        lon: '107.0682',
        status: '80 - 100%',
        address: 'Lê Thánh Tông, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },
    {
        mathung: 'TH00015',
        name: 'Điểm tập kết rác thải số 3 Đường Lê Thánh Tông',
        lat: '20.9501',
        lon: '107.0767',
        status: '80 - 100%',
        address: 'Lê Thánh Tông, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },
    {
        mathung: 'TH00017',
        name: 'Điểm tập kết rác thải số 3 Đường Lê Thánh Tông',
        lat: '20.9492',
        lon: '107.0824',
        status: '80 - 100%',
        address: 'Truyền Đăng, Bến Tàu, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00019',
        name: 'Điểm tập kết rác thải đường TQN - Lán Bè',
        lat: '20.9491',
        lon: '107.0909',
        status: '80 - 100%',
        address: 'Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00119',
        name: 'Điểm tập kết rác thải đường LTT - Lán Bè',
        lat: '20.9524',
        lon: '107.0894',
        status: '80 - 100%',
        address: 'Lê Thánh Tông, Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint2 = [{
        mathung: 'TH00013',
        name: 'Điểm tập kết rác thải đường Lán Bè Bạch Đằng',
        lat: '20.9526',
        lon: '107.0907',
        status: '< 50%',
        address: 'Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00003',
        name: 'Điểm tập kết rác thải đường Võ Nguyên Giáp',
        lat: '20.95115',
        lon: '107.10317',
        status: '< 50%',
        address: 'Võ Nguyên Giáp, Lán Bè, Hồng Hải, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00023',
        name: 'Điểm tập kết rác thải đường Văn Lang',
        lat: '20.95100',
        lon: '107.07809',
        status: '< 50%',
        address: 'Văn Lang, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00043',
        name: 'Điểm tập kết rác thải đường Bến Đoan Mỹ Gia',
        lat: '20.94859',
        lon: '107.07308',
        status: '< 50%',
        address: 'Khu Mỹ Gia, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00053',
        name: 'Điểm tập kết rác thải đường Trần Thái Tông - Nguyễn Thái Học',
        lat: '20.9626',
        lon: '107.0737',
        status: '< 50%',
        address: 'Trần Thái Tông, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint3 = [{
        mathung: 'TH00044',
        name: 'Điểm tập kết rác đường Giếng Đồn',
        lat: '20.95551',
        lon: '107.08674',
        status: '50 - 79%',
        address:'Giếng Đồn, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00040',
        name: 'Điểm tập kết rác đường Dốc Học',
        lat: '20.9501',
        lon: '107.0758',
        status: '50 - 79%',
        address:'Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00054',
        name: 'Điểm tập kết rác thải đường Nhà Thờ Hòn Gai',
        lat: '20.95319',
        lon: '107.08350',
        status: '50 - 79%',
        address: 'Nhà thờ Hòn Gai, Nhà Thờ, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        mathung: 'TH00024',
        name: 'Điểm tập kết rác thải đường Lam Sơn',
        lat: '20.9635',
        lon: '107.0789',
        status: '50 - 79%',
        address: 'Lam Sơn, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Namm'
    }];

    var lstBaiRac = [{
        name: 'Bãi rác Đèo Sen',
        lat: '20.99667',
        lon: '107.10846',
        address: 'Trần Phú, Lán Bè, Hà Khánh, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];


     map.setView([lat, lon], 16);

    var thungracredIcon = L.icon({
        iconUrl: 'images/iconMaker2.gif',
        iconSize: [35, 35], // size of the icon
    });

    var thungracgreenIcon = L.icon({
        iconUrl: 'images/iconMaker1.png',
        iconSize: [27, 27], // size of the icon
    });

    var thungracyellowIcon = L.icon({
        iconUrl: 'images/iconMaker3.png',
        iconSize: [30, 30], // size of the icon
    });

    var xechoracICon = L.icon({
        iconUrl: 'images/xechorac.png',
        iconSize: [30, 30], // size of the icon
    });

    var bairacIcon = L.icon({
        iconUrl: 'images/bairac.png',
        iconSize: [30, 30], // size of the icon
    });
    
    // var route= null;
    lstGarbagePoint1.forEach(function(elem, index) {
        
    var maker = L.marker([lstGarbagePoint1[index].lat, lstGarbagePoint1[index].lon], { icon: thungracredIcon }).on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('<b style="font-size:15px">Mã thùng: <span style="color:blue">' + lstGarbagePoint1[index].mathung+ '</span></b><br> <b>Toạ độ: </b> <a style="color: blue">' +lstGarbagePoint1[index].lat +', '+lstGarbagePoint1[index].lon+'</a><br> <b>Địa chỉ: </b>' + lstGarbagePoint1[index].address+'<br> <b>Tình trang: </b><span style="color:red; font-size:15px">' + lstGarbagePoint1[index].status+'</span>')
                .openOn(map);
        }).addTo(map);
    });

    L.Routing.control({
      waypoints: [
        L.latLng(20.99667, 107.10846), //br
        L.latLng(20.9577, 107.0838), //33
        L.latLng(20.95279, 107.07323),
        L.latLng(20.9521, 107.0807),
        L.latLng(20.9519, 107.0719),
        L.latLng(20.9563, 107.0682),
        L.latLng(20.9501, 107.0767),
        L.latLng(20.9492, 107.0824),
        L.latLng(20.9491, 107.0909),
        L.latLng(20.9524,107.0894),
    
        L.latLng(20.9590, 107.0749),
        L.latLng(20.95290, 107.09945),
        L.latLng(20.99667, 107.10846)
        
      ],addWaypoints: false,
    lineOptions: {
      styles: [{color: 'red', opacity: 1, weight: 5}]
   },createMarker: function() { return null; },
               draggableWaypoints: false,
                routeWhileDragging: true,
                router: new L.Routing.osrmv1({
    language: 'en',
    profile: 'car'
  })
    }).addTo(map);

    lstGarbagePoint2.forEach(function(elem, index) {
    var maker = L.marker([lstGarbagePoint2[index].lat, lstGarbagePoint2[index].lon], { icon: thungracgreenIcon }).on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('<b style="font-size:15px">Mã thùng: <span style="color:blue">' + lstGarbagePoint2[index].mathung+ '</span></b><br> <b>Toạ độ: </b> <a style="color: blue">' +lstGarbagePoint2[index].lat +', '+lstGarbagePoint2[index].lon+'</a><br> <b>Địa chỉ: </b>' + lstGarbagePoint2[index].address+'<br> <b>Tình trang: </b><span style="color:green; font-size:15px">' + lstGarbagePoint2[index].status+'</span>')
                .openOn(map);
        }).addTo(map);
    });

    lstGarbagePoint3.forEach(function(elem, index) {
        console.log(lstGarbagePoint3[index].lat, lstGarbagePoint3[index].lon)
    var maker = L.marker([lstGarbagePoint3[index].lat, lstGarbagePoint3[index].lon], { icon: thungracyellowIcon }).on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                .setContent('<b style="font-size:15px">Mã thùng: <span style="color:blue">' + lstGarbagePoint3[index].mathung+ '</span></b><br> <b>Toạ độ: </b> <a style="color: blue">' +lstGarbagePoint3[index].lat +', '+lstGarbagePoint3[index].lon+'</a><br> <b>Địa chỉ: </b>' + lstGarbagePoint3[index].address+'<br> <b>Tình trang: </b><span style="color:#CD950C; font-size:15px">' + lstGarbagePoint3[index].status+'</span>')
                .openOn(map);
        }).addTo(map);
    });
    L.Routing.control({
      waypoints: [
        L.latLng(20.99667, 107.10846),
        L.latLng(20.9501, 107.0758),
        L.latLng(20.9635, 107.0789),
        L.latLng(20.95319, 107.08350),
        L.latLng(20.95551,107.08674),
        L.latLng(20.99667, 107.10846)
 
      ],addWaypoints: false,
    lineOptions: {
      styles: [{color: '#CD950C', opacity: 1, weight: 5}]
   },createMarker: function() { return null; },
               draggableWaypoints: false,
                routeWhileDragging: true,
                router: new L.Routing.osrmv1({
    language: 'en',
    profile: 'car'
  })
    }).addTo(map);
    // var yellowRoute = new L.Routing.control({
    //     waypoints: [ 
    //         L.latLng(20.9519 ,107.0719), 
    //         L.latLng(20.95290, 107.09945), 
    //         L.latLng(20.9521, 107.0807), 
    //         L.latLng(20.95279, 107.07323),       
    //         L.latLng(20.99667,107.10846)
    //     ],
    //     addWaypoints: false,
    //     lineOptions: {
    //       styles: [{color: '#CD950C', opacity: 1, weight: 5}]
    //    },
    //    draggableWaypoints: false,
    //     routeWhileDragging: true
    // }).addTo(map);

    for (var ix = 0; ix < lstBaiRac.length; ix++) {
        var maker = L.marker([lstBaiRac[ix].lat, lstBaiRac[ix].lon], { icon: bairacIcon }).addTo(map);
    }



    
 
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
